/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import beans.ExpressionOccurrence;
import dialogs.StatisticsFrame;
import dialogs.TextsDialog;
import dialogs.VowelsDialog;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author koutsch
 */
public class MainFrame extends javax.swing.JFrame {

    Logic log = new Logic();
    ConcModel model = new ConcModel();
    DefaultTableModel concStrsModel = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int row, int column) {
            return false;   //This causes all cells to be not editable
        }

        // makes that words are ordered alphabetically and occurrences acc. to their number
        @Override
        public Class getColumnClass(int column) {

            if (column % 2 == 0) {

                return String.class;
            } else {

                return Integer.class;
            }
        }
    };

    TableColumnAdjuster tca;
    DefaultTableCellRenderer concRenderer;
    LeftDotRenderer beforeRenderer;
    ConcordanceWorker cw;
    boolean punctuationConsidered;
    ConcTable resultTable;
    JTable[] concTables;
    DefaultTableModel[] concTableModels;
    int concTableCnt;
    private String searchText;
    private StatisticsFrame sf;
    private TableColumn fileColumn;
    private TableColumn indexColumn;

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {

        // Set icon
        URL iconURL = getClass().getResource("/main/Concordator.png");
        ImageIcon icon = new ImageIcon(iconURL);
        this.setIconImage(icon.getImage());

        // initiate GUI
        initComponents();

        setTitle(this.getClass().getPackage().getImplementationTitle() + " " + this.getClass().getPackage().getImplementationVersion());

        // set resultTable
        resultTable = new ConcTable();
        // resultTable.setModel(model);
        resultScrollPane.setViewportView(resultTable);

        resultTable.addMouseListener(ma);

        // save used files on close
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    log.writeChosenToFile();
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null, "Chosen Files could not be saved!\n"
                            + ex, "Error", JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        // set keyboard shortcuts
        setShortcuts();

        // set left-right character length spinner (default value 120 characters)
        SpinnerModel cntSpinMod = new SpinnerNumberModel(120, 0, 1000, 1);
        countSpinner.setModel(cntSpinMod);

        // set string conc amount
        SpinnerModel conSpinMod = new SpinnerNumberModel(3, 1, 10, 1);
        concSpinner.setModel(conSpinMod);

        try {
            // read files to be used in concordator
            ArrayList<String> chosenFromFile = log.getChosenFromFile();

            if (chosenFromFile.size() > 0) {

                String message = "The following files could not be found:\n";
                for (String cff : chosenFromFile) {

                    message += cff + "\n";
                }
                JOptionPane.showMessageDialog(null, message,
                        "File missing...", JOptionPane.ERROR_MESSAGE);
            }
        } catch (IOException | ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Login information can not be read!\n"
                    + ex, "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            String savedVowels = log.getSavedVowels();
            log.setVowels(savedVowels);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Saved vowels can not be read!\n"
                + ex, "Error", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }

        // set the location of the horizontal divider between result table an concordance occurrence table
        splitPane.setDividerLocation((splitPane.getHeight() / 5) * 4);
    }

    MouseAdapter ma = new MouseAdapter() {

        @Override
        public void mouseClicked(MouseEvent e) {

            if (e.getButton() == MouseEvent.BUTTON3) {

                int r = resultTable.rowAtPoint(e.getPoint());
                int c = resultTable.columnAtPoint(e.getPoint());
                if (r >= 0 && r < resultTable.getRowCount() && c >= 0 && c < resultTable.getColumnCount()) {
                    resultTable.setRowSelectionInterval(r, r);
                    resultTable.setColumnSelectionInterval(c, c);

                    JPopupMenu popup = new JPopupMenu();

                    JMenuItem item;
                    popup.add(item = new JMenuItem("Copy selected cell"));
                    item.setHorizontalTextPosition(JMenuItem.RIGHT);
                    item.addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent ae) {

                            int selectedRow = resultTable.getSelectedRow();
                            int convertRowIndexToModel = resultTable.convertRowIndexToModel(selectedRow);

                            int selectedColumn = resultTable.getSelectedColumn();
                            int convertColumnIndexToModel = resultTable.convertColumnIndexToModel(selectedColumn);

                            Object valueAt = model.getValueAt(convertRowIndexToModel, convertColumnIndexToModel);

                            Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
                            StringSelection ss = new StringSelection(valueAt.toString());
                            cb.setContents(ss, null);
                        }
                    });
                    popup.add(item = new JMenuItem("Copy row"));
                    item.setHorizontalTextPosition(JMenuItem.RIGHT);
                    item.addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent ae) {
                            int selectedRow = resultTable.getSelectedRow();
                            int convertRowIndexToModel = resultTable.convertRowIndexToModel(selectedRow);

                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < model.getColumnCount(); i++) {

                                sb.append(model.getValueAt(convertRowIndexToModel, i));
                            }

                            Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
                            StringSelection ss = new StringSelection(sb.toString());
                            cb.setContents(ss, null);
                        }
                    });

                    popup.setLabel("Copy");

                    popup.show(resultTable, e.getX(), e.getY());

                } else {
                    resultTable.clearSelection();
                }
            }
        }
    };

    public JTable[] getConcTables() {

        return concTables;
    }

    public int getConcTableCnt() {

        return concTableCnt;
    }

    public DefaultTableModel[] getConcTableModels() {

        return concTableModels;
    }

    /**
     * Get the result table.
     *
     * @return the JTable holding the concordance results
     */
    public JTable getTable() {

        return resultTable;
    }

    public Logic getLogic() {

        return log;
    }

    /**
     * Get the table model of the result table.
     *
     * @return a DefaultTableModel assigned to the result table
     */
    public ConcModel getTableModel() {

        return (ConcModel) resultTable.getModel();
    }

    /**
     * Get the text of the search field.
     *
     * @return a String holding the text the user entered into the text field in
     * the concordator
     */
    public String getFieldText() {

        return searchTextField.getText();
    }

    public void setStateLabel(int resCnt) {

        stateLabel.setText(resCnt + " left.");
    }

    /**
     * Set the keyboard shortcuts for the concordator (Search with "Enter").
     */
    private void setShortcuts() {

        KeyStroke mainUpdateStroke = KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ENTER, 0);
        searchButton.setToolTipText("Start concordance analysis");
        searchButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(mainUpdateStroke, "search");
        searchButton.getActionMap().put("search",
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        search();
                    }
                });
    }

    /**
     * Add a file to the list of files to work with.
     */
    private void addFile() {

        JFileChooser fc = new JFileChooser();
        fc.setMultiSelectionEnabled(true);
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Text Files", "txt");
        fc.setFileFilter(filter);
        int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            log.addFiles(fc.getSelectedFiles());
        }
    }

    public void setConcTables() {

        concPanel.setLayout(new BoxLayout(concPanel, BoxLayout.X_AXIS));

        concTableCnt = (Integer) concSpinner.getValue();

        concTables = new JTable[concTableCnt * 2];
        concTableModels = new DefaultTableModel[concTableCnt * 2];

        for (int i = 0; i < concTables.length; i++) {

            concTableModels[i] = new DefaultTableModel() {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;   //This causes all cells to be not editable
                }

                // makes that words are ordered alphabetically and occurrences acc. to their number
                @Override
                public Class getColumnClass(int column) {

                    if (column % 2 == 0) {

                        return String.class;
                    } else {

                        return Integer.class;
                    }
                }
            };

            concTables[i] = new JTable();
            concTables[i].getTableHeader().setReorderingAllowed(false);

            JScrollPane scrollPane = new JScrollPane();
            scrollPane.setViewportView(concTables[i]);
            concPanel.add(scrollPane);
        }
        concPanel.validate();
    }

    private void setConcTablesEnabled(boolean enabled) {
        for (JTable concTable : concTables) {
            concTable.setEnabled(enabled);
        }
    }

    /**
     * Search for a concordance. This method opens a SwingWorker to search for
     * concordances so that the user has control over the process and can cancel
     * it. After search the results are displayed in the result and the
     * concordance occurrence tables.
     *
     */
    public void search() {

        if (!searchTextField.getText().isEmpty()) {

            searchText = searchTextField.getText();

            if (log.getChosenCount() > 0) {

                resultTable.setModel(new DefaultTableModel());

                model.prepareColumns();

                // Delete all conc tables
                concPanel.removeAll();

                // Add new conc tables
                setConcTables();

                // Disable the main and conc tables while searching
                resultTable.setEnabled(false);
                setConcTablesEnabled(false);

                searchButton.setText("Cancel");

                rToLToggleButton.setSelected(false);

                stateLabel.setText("Calculating...");

                progressBar.setValue(0);

                for (int i = 0; i < concTableCnt; i++) {

                    int left = (concTableCnt - 1) - i;

                    int number = concTableCnt - left;
                    String ordin;

                    switch (number) {

                        case 1:
                            ordin = "st";
                            break;
                        case 2:
                            ordin = "nd";
                            break;
                        case 3:
                            ordin = "rd";
                            break;
                        default:
                            ordin = "th";
                            break;
                    }

                    ordin = number + ordin;

                    concTableModels[left].addColumn(ordin + " left");
                    concTableModels[left].addColumn("Occ Count");

                    int right = concTableCnt + i;
                    concTableModels[right].addColumn(ordin + " right");
                    concTableModels[right].addColumn("Occ Count");
                }

                cw = new ConcordanceWorker(searchText, log,
                        countSpinner, eowCheckBox.isSelected(), caseInsensitiveCheckBox.isSelected(),
                        concTableCnt, model, concTableModels);

                cw.addPropertyChangeListener(new PropertyChangeListener() {

                    @Override
                    public void propertyChange(PropertyChangeEvent evt) {
                        switch (evt.getPropertyName()) {
                            case "progress":
                                progressBar.setValue((Integer) evt.getNewValue());
                                break;
                            case "state":
                                switch ((SwingWorker.StateValue) evt.getNewValue()) {
                                    case DONE:
                                        try {

                                            // get result count
                                            Integer result = cw.get();

                                            int rowCnt = model.getRowCount();
                                            boolean proceed = true;
                                            if (rowCnt > 100000) {

                                                proceed = JOptionPane.showConfirmDialog(null,
                                                        "You search gives " + rowCnt + " results.\n"
                                                        + "Displaying them may lead to a programme freeze! \n\n"
                                                        + "Do you really want to continue?", "WARNING",
                                                        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
                                            }

                                            if (proceed) {
                                                
                                                resultTable.setModel(model);
                                                fileColumn = resultTable.getColumn("File");
                                                resultTable.removeColumn(fileColumn);
                                                indexColumn = resultTable.getColumn("Index");
                                                resultTable.removeColumn(indexColumn);                                                
                                                
                                                if (positionMenuItem.isSelected()) {

                                                    resultTable.addColumn(fileColumn);
                                                    resultTable.addColumn(indexColumn);

                                                }

                                                for (int i = 0; i < concTableModels.length; i++) {

                                                    concTables[i].setModel(concTableModels[i]);
                                                }

                                                resultTable.setColumns();
                                                punctuationConsidered = tpicCheckBox.isSelected();
                                                resultTable.setRowSorters(punctuationConsidered);
                                                resultTable.setRenderers();

                                                stateLabel.setText("Pattern found " + result + " times.");
                                                if (resultTable.getRowCount() > 0) {

                                                    // allow user to interact when everything is done
                                                    rToLToggleButton.setEnabled(true);
                                                    statisticsButton.setEnabled(true);
                                                    resultTable.setEnabled(true);
                                                    setConcTablesEnabled(true);
                                                    toFileMenuItem.setEnabled(true);
                                                } else {

                                                    statisticsButton.setEnabled(false);
                                                }

                                                progressBar.setValue(0);

                                                // "format" the conc tables
                                                for (JTable concTable : concTables) {
                                                    concTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
                                                    TableColumnAdjuster tcaC = new TableColumnAdjuster(concTable);
                                                    tcaC.adjustColumns();

                                                    concTable.setAutoCreateRowSorter(true);
                                                }

                                                // getStatistics if already open
                                                if (sf != null) {

                                                    ArrayList<ExpressionOccurrence> statistics = log.getStatistics(model);
                                                    sf.getStatistics(statistics);
                                                }
                                            } else {

                                                resultTable.setEnabled(true);
                                                setConcTablesEnabled(true);
                                                progressBar.setValue(0);
                                                stateLabel.setText("Search cancelled!");
                                                rToLToggleButton.setEnabled(false);
                                                statisticsButton.setEnabled(false);
                                            }

                                        } catch (final CancellationException e) {
                                            JOptionPane.showMessageDialog(null, "The process was cancelled", "Search Matches",
                                                    JOptionPane.WARNING_MESSAGE);
                                            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, e);
                                            resultTable.setEnabled(true);
                                            setConcTablesEnabled(true);
                                            progressBar.setValue(0);
                                            stateLabel.setText("Search cancelled!");
                                        } catch (InterruptedException ex) {
                                            JOptionPane.showMessageDialog(null, "The process was interrupted", "Search Matches",
                                                    JOptionPane.WARNING_MESSAGE);
                                            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);

                                            model.setRowCount(0);

                                            resultTable.setEnabled(true);
                                            setConcTablesEnabled(true);
                                            progressBar.setValue(0);
                                            stateLabel.setText("Aborted...");
                                        } catch (ExecutionException ex) {
                                            JOptionPane.showMessageDialog(null, "The process was not executed\n" + ex, "Search Matches",
                                                    JOptionPane.WARNING_MESSAGE);
                                            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                                            resultTable.setEnabled(true);
                                            setConcTablesEnabled(true);
                                            progressBar.setValue(0);
                                            stateLabel.setText("Error...");
                                        } finally {
                                            searchButton.setText("Search");
                                        }
                                }
                        }
                    }
                });
                cw.execute();
            } else {

                JOptionPane.showMessageDialog(rootPane, "No files to search selected!\n");
            }
        } else {

            JOptionPane.showMessageDialog(rootPane, "No search key specified!\n");
        }
    }

    /**
     * Opens a help menu explaining the regular expressions.
     */
    private void regexHelp() {

        HelpFrame help = new HelpFrame("regex.html");
        help.setTitle("Regular Expressions");
    }

    /**
     * Opens a help menu explaining the program functions.
     */
    private void workHelp() {

        HelpFrame help = new HelpFrame("help.html");
        help.setTitle("Help");
    }

    /**
     * Cancel a concordance search.
     */
    private void cancel() {
        cw.cancel(true);
    }

    public void deleteSelected(String[] exclude) {

        int newCnt = model.getRowCount();
        if (exclude == null) {

        } else {

            for (int i = newCnt - 1; i >= 0; i--) {

                for (String ex : exclude) {

                    if (ex.equals(model.getValueAt(i, 1))) {

                        model.removeRow(i);
                        newCnt--;
                    }
                }
            }
        }

        stateLabel.setText(newCnt + " left.");

        for (int i = 0; i < concTables.length; i++) {

            concTables[i].setModel(new DefaultTableModel());
            concTableModels[i].setRowCount(0);
        }

        cw = new ConcordanceWorker(log,
                concTableCnt, model, concTableModels);
        cw.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                switch (evt.getPropertyName()) {

                    case "state":
                        switch ((SwingWorker.StateValue) evt.getNewValue()) {
                            case DONE:
                                for (int i = 0; i < concTableModels.length; i++) {

                                    concTables[i].setModel(concTableModels[i]);
                                }
                        }
                }
            }
        });
        cw.execute();

        // getStatistics if already open
        if (sf != null) {

            ArrayList<ExpressionOccurrence> statistics = log.getStatistics(model);
            sf.getStatistics(statistics);

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        searchTextField = new javax.swing.JTextField();
        searchButton = new javax.swing.JButton();
        countSpinner = new javax.swing.JSpinner();
        stateLabel = new javax.swing.JLabel();
        statisticsButton = new javax.swing.JButton();
        progressBar = new javax.swing.JProgressBar();
        eowCheckBox = new javax.swing.JCheckBox();
        caseInsensitiveCheckBox = new javax.swing.JCheckBox();
        splitPane = new javax.swing.JSplitPane();
        concPanel = new javax.swing.JPanel();
        resultScrollPane = new javax.swing.JScrollPane();
        tpicCheckBox = new javax.swing.JCheckBox();
        deleteButton = new javax.swing.JButton();
        concSpinner = new javax.swing.JSpinner();
        rToLToggleButton = new javax.swing.JToggleButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        addFileMenuItem = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        changeVowelsMenuItem = new javax.swing.JMenuItem();
        textMenu = new javax.swing.JMenu();
        chooseTextsMenuItem = new javax.swing.JMenuItem();
        positionMenuItem = new javax.swing.JCheckBoxMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        toFileMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        helpMenuItem = new javax.swing.JMenuItem();

        jMenuItem1.setText("jMenuItem1");

        jMenuItem3.setText("jMenuItem3");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(800, 600));

        searchTextField.setToolTipText("Enter search expression here");

        searchButton.setText("Search");
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });

        countSpinner.setToolTipText("Number of characters left and right of the result to be displayed");

        stateLabel.setText("Concordator");

        statisticsButton.setText("Statistics");
        statisticsButton.setEnabled(false);
        statisticsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                statisticsButtonActionPerformed(evt);
            }
        });

        eowCheckBox.setText("EOW");
        eowCheckBox.setToolTipText("If selected the entered search expression is interpreted as a whole word");

        caseInsensitiveCheckBox.setText("CI");
        caseInsensitiveCheckBox.setToolTipText("Case insensitive");

        splitPane.setDividerSize(20);
        splitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        javax.swing.GroupLayout concPanelLayout = new javax.swing.GroupLayout(concPanel);
        concPanel.setLayout(concPanelLayout);
        concPanelLayout.setHorizontalGroup(
            concPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 776, Short.MAX_VALUE)
        );
        concPanelLayout.setVerticalGroup(
            concPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 317, Short.MAX_VALUE)
        );

        splitPane.setRightComponent(concPanel);
        splitPane.setLeftComponent(resultScrollPane);

        tpicCheckBox.setText("TPIC");
        tpicCheckBox.setToolTipText("Take punctuation into consideration when sorting");
        tpicCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tpicCheckBoxActionPerformed(evt);
            }
        });

        deleteButton.setText("Delete");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        concSpinner.setToolTipText("Number of words left and right of the result");

        rToLToggleButton.setText("العربية עברית ‎");
        rToLToggleButton.setToolTipText("Change to right-to-left for certain languages");
        rToLToggleButton.setEnabled(false);
        rToLToggleButton.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rToLToggleButtonItemStateChanged(evt);
            }
        });

        jMenu1.setText("File");

        addFileMenuItem.setText("Add file...");
        addFileMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addFileMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(addFileMenuItem);
        jMenu1.add(jSeparator2);

        changeVowelsMenuItem.setText("Change vowels...");
        changeVowelsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeVowelsMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(changeVowelsMenuItem);

        jMenuBar1.add(jMenu1);

        textMenu.setText("Texts");

        chooseTextsMenuItem.setText("Choose Texts...");
        chooseTextsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseTextsMenuItemActionPerformed(evt);
            }
        });
        textMenu.add(chooseTextsMenuItem);

        positionMenuItem.setText("Show File and Position");
        positionMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                positionMenuItemActionPerformed(evt);
            }
        });
        textMenu.add(positionMenuItem);
        textMenu.add(jSeparator1);

        toFileMenuItem.setText("Print to XML...");
        toFileMenuItem.setEnabled(false);
        toFileMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                toFileMenuItemActionPerformed(evt);
            }
        });
        textMenu.add(toFileMenuItem);

        jMenuBar1.add(textMenu);

        helpMenu.setText("Help");

        jMenuItem2.setText("Regular Expressions...");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        helpMenu.add(jMenuItem2);

        helpMenuItem.setText("Help...");
        helpMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                helpMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(helpMenuItem);

        jMenuBar1.add(helpMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(splitPane)
                    .addComponent(searchTextField, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(stateLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(deleteButton))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(searchButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(countSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(eowCheckBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(caseInsensitiveCheckBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tpicCheckBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(concSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rToLToggleButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(statisticsButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(deleteButton))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(searchTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(searchButton)
                            .addComponent(countSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(statisticsButton)
                            .addComponent(eowCheckBox)
                            .addComponent(caseInsensitiveCheckBox)
                            .addComponent(tpicCheckBox)
                            .addComponent(concSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rToLToggleButton))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(splitPane, javax.swing.GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(stateLabel)
                            .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
        // TODO add your handling code here:
        if (searchButton.getText().equals("Search")) {

            search();
        } else {

            cancel();
        }
    }//GEN-LAST:event_searchButtonActionPerformed

    private void addFileMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addFileMenuItemActionPerformed
        // TODO add your handling code here:
        addFile();
    }//GEN-LAST:event_addFileMenuItemActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        regexHelp();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void helpMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_helpMenuItemActionPerformed
        // TODO add your handling code here:
        workHelp();
    }//GEN-LAST:event_helpMenuItemActionPerformed

    private void chooseTextsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseTextsMenuItemActionPerformed
        // TODO add your handling code here:
        TextsDialog td = new TextsDialog(this, true);
    }//GEN-LAST:event_chooseTextsMenuItemActionPerformed

    private void statisticsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_statisticsButtonActionPerformed
        // TODO add your handling code here:
        ArrayList<ExpressionOccurrence> statistics = log.getStatistics(model);

        if (sf == null) {

            sf = new StatisticsFrame(this, statistics);
            sf.addWindowListener(new WindowAdapter() {

                @Override
                public void windowClosing(WindowEvent e) {

                    sf = null;
                }
            });
            sf.okButton.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    sf.dispose();
                    sf = null;
                }
            });
        } else {

            sf.getStatistics(statistics);
            sf.setVisible(true);
        }
    }//GEN-LAST:event_statisticsButtonActionPerformed

    private void tpicCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tpicCheckBoxActionPerformed
        // TODO add your handling code here:
        if (model.getColumnCount() > 0) {
            punctuationConsidered = tpicCheckBox.isSelected();
            resultTable.setRowSorters(punctuationConsidered);
        }
    }//GEN-LAST:event_tpicCheckBoxActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        // TODO add your handling code here:
        int[] selectedRows = resultTable.getSelectedRows();
        if (selectedRows.length > 0) {

            for (int i = selectedRows.length; i > 0; i--) {

                int convertRowIndexToModel = resultTable.convertRowIndexToModel(selectedRows[i - 1]);
                model.removeRow(convertRowIndexToModel);
            }

            deleteSelected(null);
        }
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void rToLToggleButtonItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rToLToggleButtonItemStateChanged
        // TODO add your handling code here:
        resultTable.changeDirection(rToLToggleButton.isSelected());
    }//GEN-LAST:event_rToLToggleButtonItemStateChanged

    private void toFileMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_toFileMenuItemActionPerformed
        try {
            // TODO add your handling code here:
            log.toXML(model, searchText, this);
        } catch (IOException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(this, "File could not be written!\n"
                    + ex, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_toFileMenuItemActionPerformed

    private void positionMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_positionMenuItemActionPerformed
        // TODO add your handling code here:
        if (model.getColumnCount() >= 3) {

            if (positionMenuItem.isSelected()) {

                resultTable.addColumn(fileColumn);
                resultTable.addColumn(indexColumn);

            } else {

                fileColumn = resultTable.getColumn("File");
                resultTable.removeColumn(fileColumn);
                indexColumn = resultTable.getColumn("Index");
                resultTable.removeColumn(indexColumn);

            }
        } else {

            positionMenuItem.setSelected(false);
        }
    }//GEN-LAST:event_positionMenuItemActionPerformed

    private void changeVowelsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changeVowelsMenuItemActionPerformed
        // TODO add your handling code here:
        VowelsDialog vd = new VowelsDialog(this, true);
    }//GEN-LAST:event_changeVowelsMenuItemActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem addFileMenuItem;
    private javax.swing.JCheckBox caseInsensitiveCheckBox;
    private javax.swing.JMenuItem changeVowelsMenuItem;
    private javax.swing.JMenuItem chooseTextsMenuItem;
    private javax.swing.JPanel concPanel;
    private javax.swing.JSpinner concSpinner;
    private javax.swing.JSpinner countSpinner;
    private javax.swing.JButton deleteButton;
    private javax.swing.JCheckBox eowCheckBox;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JMenuItem helpMenuItem;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JCheckBoxMenuItem positionMenuItem;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JToggleButton rToLToggleButton;
    private javax.swing.JScrollPane resultScrollPane;
    private javax.swing.JButton searchButton;
    private javax.swing.JTextField searchTextField;
    private javax.swing.JSplitPane splitPane;
    private javax.swing.JLabel stateLabel;
    private javax.swing.JButton statisticsButton;
    private javax.swing.JMenu textMenu;
    private javax.swing.JMenuItem toFileMenuItem;
    private javax.swing.JCheckBox tpicCheckBox;
    // End of variables declaration//GEN-END:variables
}
