/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import beans.ExpressionOccurrence;
import beans.FileCollection;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author koutsch
 */
public class Logic {

    String text;
    String fileName;

    ArrayList<File> files = new ArrayList<>();
    ArrayList<Boolean> chosen = new ArrayList<>();

    ArrayList<Object[]> rows;

    final String vowels = "[AEIOUaeiouАЄЕЁИІЇОУЫЭЮЯаеиоуыэюяёєіїΑΕΗΙΟΥΩαεηιουω]";
    final String consonants = "[^\\W0-9AEIOUaeiouАЄЕЁИІЇОУЫЭЮЯаеиоуыэюяёєіїΑΕΗΙΟΥΩαεηιουω]";

    private String vowel = vowels;
    private String consonant = consonants;

    private Matcher matcher;

    private final Pattern wordString = Pattern.compile("[\\p{InCombiningDiacriticalMarks}|\\p{L}]+", Pattern.UNICODE_CHARACTER_CLASS);

    private Matcher reverseMatch;
    private Matcher forwardMatch;
    private final String FILEPATH = System.getProperty("user.home") + File.separator;
    private final String loaded = "Concordator " + this.getClass().getPackage().getImplementationVersion() + " started.";
    private boolean eow;

    public Logic() {

        System.out.println(loaded);
        System.out.println("User Home Directory: " + FILEPATH);
    }

    /**
     * Add new vowels when using another language. Default is only [AEIOUaeiou] + greek and cyrillic.
     *
     * @param newVowels a String containing the vowels
     */
    public void addVowels(String newVowels) {
        
        String upper = newVowels.toUpperCase(new Locale("UTF-8"));
        if (!newVowels.equals(upper)) {
            
            newVowels += upper;
        }

        vowel = vowel.substring(0, vowel.length() - 1) + newVowels + "]";
        consonant = consonant.substring(0, consonant.length() - 1) + newVowels + "]";
    }
    
    public void setVowels(String vowels) {
        
        vowel = vowels;
        System.out.println("Vowels saved: " + vowels);
        consonant = "[^\\W0-9" + vowels.substring(1);
        System.out.println("Consonant declaration: " + consonant);
    }

    /**
     * Reset vowels to initial state
     */
    public void resetVowels() {

        vowel = vowels;
        consonant = consonants;
    }

    /**
     * The current vowel definition.
     *
     * @return a String containing all letters currently considered as vowel
     */
    public String getVowels() {
        return vowel;
    }
    
    /**
     * The basic vowel definition.
     *
     * @return a String containing all basic vowels
     */
    public String getBasicVowels() {
        return vowels.substring(1, vowels.length() - 1);
    } 

    /**
     * Get the files that are saved.
     *
     * @return an ArrayList with the file objects
     */
    public ArrayList<File> getFiles() {

        return files;
    }

    /**
     * Get the list of booleans determining if a file is chosen to be worked
     * with.
     *
     * @return a ArrayList of booleans assigned to every saved file
     */
    public ArrayList<Boolean> getChosen() {

        return chosen;
    }

    public void addFiles(File[] selectedFiles) {

        for (File selectedFile : selectedFiles) {
            files.add(selectedFile);
            chosen.add(true);
        }
    }

    /**
     * Get the number of chosen items.
     *
     * @return an integer representing the number of chosen files
     */
    public int getChosenCount() {

        int chosenCount = 0;
        for (boolean chosenItem : chosen) {

            if (chosenItem) {

                chosenCount++;
            }
        }
        return chosenCount;
    }

    /*
     * Write the addresses of the files used to an extra file so they can be
     * used when launching concordator again.
     */
    public void writeChosenToFile() throws FileNotFoundException, IOException {

        String[] paths = new String[files.size()];
        for (int i = 0; i < files.size(); i++) {

            paths[i] = files.get(i).getAbsolutePath();
        }

        File concDir = new File(FILEPATH + "Concordator");
        if (!concDir.exists()) {

            concDir.mkdir();
        }

        FileCollection fc = new FileCollection(paths);
        FileOutputStream fos = new FileOutputStream(FILEPATH + "Concordator" + File.separator + "chosenFiles");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(fc);
        
        File vowelsFile = new File(FILEPATH + "Concordator" + File.separator + "vowels");
        try (FileWriter w = new FileWriter(vowelsFile)) {
            w.write(vowel);
            w.flush();
        }
    }
    
    public String getSavedVowels() throws FileNotFoundException, IOException {
        
        File vowelsFile = new File(FILEPATH + "Concordator" + File.separator + "vowels");
        return new String(Files.readAllBytes(vowelsFile.toPath()));
    }

    public ArrayList<String> getChosenFromFile() throws FileNotFoundException, IOException, ClassNotFoundException {

        FileInputStream fis = new FileInputStream(FILEPATH + "Concordator" + File.separator + "chosenFiles");
        ObjectInputStream ois = new ObjectInputStream(fis);
        FileCollection fc = (FileCollection) ois.readObject();
        ArrayList<String> notFound = new ArrayList<>();

        if (fc != null) {

            String[] savedFilePaths = fc.getFiles();

            for (String savedFile : savedFilePaths) {

                File file = new File(savedFile);

                if (file.exists()) {

                    files.add(file);
                    chosen.add(true);
                } else {

                    notFound.add(savedFile);
                }
            }
        }
        return notFound;
    }

    /**
     * Get the text from a text file.
     *
     * @param index
     * @throws IOException if the file cannot be read
     */
    public void getText(int index) throws IOException {

        byte[] encoded = Files.readAllBytes(files.get(index).toPath());
        text = StandardCharsets.UTF_8.decode(ByteBuffer.wrap(encoded)).toString();
        fileName = files.get(index).getName();
        reverseMatch = wordString.matcher(new StringBuilder(text).reverse());
        forwardMatch = wordString.matcher(text);
    }

    public String getFileName() {

        return fileName;
    }

    public int getTextLength() {

        if (text != null) {

            return text.length();
        } else {

            return 0;
        }
    }

    public Matcher getMatch(String patternStr, boolean eow, boolean ci) {

        this.eow = eow;

        if (eow) {

            patternStr = "[^\\p{InCombiningDiacriticalMarks}|\\p{L}]" + patternStr + "[^\\p{InCombiningDiacriticalMarks}|\\p{L}]";
        }

        patternStr = patternStr.replace("?", "[\\p{InCombiningDiacriticalMarks}|\\p{L}]?");
        patternStr = patternStr.replace("+", "[\\p{InCombiningDiacriticalMarks}|\\p{L}]+");
        patternStr = patternStr.replace("*", "[\\p{InCombiningDiacriticalMarks}|\\p{L}]*");
        patternStr = patternStr.replace(".", "[\\p{InCombiningDiacriticalMarks}|\\p{L}]");

        patternStr = patternStr.replace("/V", vowel);
        patternStr = patternStr.replace("/C", consonant);

        // search for sentence marks
        patternStr = patternStr.replace("/ask", "\\?");
        patternStr = patternStr.replace("/dot", "\\.");
        patternStr = patternStr.replace("/exc", "\\!");

        int flags;
        if (ci) {

            flags = Pattern.UNICODE_CHARACTER_CLASS | Pattern.CASE_INSENSITIVE;
        } else {

            flags = Pattern.UNICODE_CHARACTER_CLASS;
        }

        Pattern pattern = Pattern.compile(patternStr, flags);

        matcher = pattern.matcher(text);
        return matcher;
    }

    public Object[] getConcRow(int leftRight, int index) {

        if (eow && index > 0) {

            index--;
        }

        int corrLeft = 0;
        if (leftRight > matcher.start()) {

            corrLeft = leftRight - matcher.start();
        }

        int corrRight = 0;
        if (leftRight > (text.length() - matcher.end())) {

            corrRight = leftRight - (text.length() - matcher.end());
        }

        String match = text.substring(matcher.start(), matcher.end()).replaceAll("\\r\\n|\\r|\\n", " ");

        Object[] row = new Object[5];

        int preEnd = matcher.start();
        int postBegin = matcher.end();
        if (eow) {

            preEnd++;
            postBegin--;

            match = match.substring(1, match.length() - 1);
        }

        row[0] = text.substring(matcher.start() - leftRight + corrLeft, preEnd).replaceAll("\\r\\n|\\r|\\n", " ");
        row[1] = match;
        row[2] = text.substring(postBegin, matcher.end() + leftRight - corrRight).replaceAll("\\r\\n|\\r|\\n", " ");
        row[3] = fileName;
        row[4] = matcher.start();

        return row;

    }

    public Object[] getCoocStrings(int begin, int end, int strCnt) {

        Object[] strings = new Object[strCnt * 2];

        int revIndex = text.length() - begin;
        for (int i = strCnt - 1; i >= 0 && reverseMatch.find(revIndex); i--) {

            strings[i] = text.substring(text.length() - reverseMatch.end(), text.length() - reverseMatch.start());

            revIndex = reverseMatch.end();
        }

        for (int i = strCnt; i < strCnt * 2 && forwardMatch.find(end); i++) {

            strings[i] = text.substring(forwardMatch.start(), forwardMatch.end());

            end = forwardMatch.end();
        }

        return strings;
    }

    /**
     * Get the types and their occurrences from a list of Strings.
     *
     * @param lists the list holding the Strings to compare
     * @param column
     * @return a ExpressionOccurrence[] holding each the type and its number of
     * occurrences
     */
    public ArrayList<ExpressionOccurrence> getOccurrences(ArrayList<Object[]> lists, int column) {

        ArrayList<ExpressionOccurrence> occurrences = new ArrayList<>();

        for (Object[] subList : lists) {
            boolean has = false;
            for (ExpressionOccurrence occurrence : occurrences) {

                if (occurrence.getExpression() != null && subList[column] != null) {

                    if (occurrence.getExpression().equals(subList[column])) {
                        occurrence.increaseCount(1);
                        has = true;
                        break;
                    }
                }

            }
            if (has == false) {

                if (subList[column] != null) {

                    ExpressionOccurrence occurrence = new ExpressionOccurrence((String) subList[column], 1);
                    occurrences.add(occurrence);
                }
            }
        }
        return occurrences;
    }

    public ArrayList<ExpressionOccurrence> getStatistics(DefaultTableModel model) {

        ArrayList<ExpressionOccurrence> occurrences = new ArrayList<>();

        for (int i = 0; i < model.getRowCount(); i++) {
            Object valueAt = model.getValueAt(i, 1);
            String value = String.valueOf(valueAt);

            boolean has = false;
            for (ExpressionOccurrence occurrence : occurrences) {
                if (occurrence.getExpression().equals(value)) {
                    occurrence.increaseCount(1);
                    has = true;
                    break;
                }
            }

            if (has == false) {

                ExpressionOccurrence occurrence = new ExpressionOccurrence(value, 1);
                occurrences.add(occurrence);
            }
        }
        return occurrences;
    }

    public void toXML(ConcModel model, String concEx, JFrame parent) throws IOException {

        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showSaveDialog(parent) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            try (Writer out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(file), "UTF8"))) {

                out.append("<?xml version='1.0' encoding='UTF-8' ?>\n");

                out.append("<conc>\n");
                out.append("<conc_ex>");
                out.append(concEx);
                out.append("</conc_ex>\n");

                for (int i = 0; i < model.getRowCount(); i++) {

                    for (int j = 0; j < model.getColumnCount(); j++) {

                        switch (j) {

                            case 0:
                                out.append("<before>");
                                out.append(model.getValueAt(i, j).toString());
                                out.append("</before>\n");
                                break;
                            case 1:
                                out.append("<match>");
                                out.append(model.getValueAt(i, j).toString());
                                out.append("</match>\n");
                                break;
                            case 2:
                                out.append("<after>");
                                out.append(model.getValueAt(i, j).toString());
                                out.append("</after>\n");
                                break;
                            case 3:
                                out.append("<file_at>");
                                out.append(model.getValueAt(i, j).toString());
                                out.append("</file_at>\n");
                                break;
                        }
                    }
                }
                out.append("</conc>");
                out.flush();
            }
        }
    }
}
