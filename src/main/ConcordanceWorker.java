/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import beans.ExpressionOccurrence;
import java.util.ArrayList;
import java.util.regex.Matcher;
import javax.swing.JSpinner;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author koutsch
 */
public class ConcordanceWorker extends SwingWorker<Integer, String> {

    private static void failIfInterrupted() throws InterruptedException {
        if (Thread.currentThread().isInterrupted()) {
            throw new InterruptedException("Interrupted while searching");
        }
    }
    private String patternStr;
    private final Logic log;

    private JSpinner countSpinner;
    private boolean eow;
    private boolean ci;
    private final int concStrCount;
    private final ConcModel model;
    private final DefaultTableModel[] concTableModels;
    private ArrayList<Object[]> coocs;

    private final boolean skipMatch;

    /**
     *
     * @param patternStr
     * @param log
     * @param countSpinner
     * @param eow
     * @param ci
     * @param concStrCount
     * @param model
     * @param concTableModels
     */
    public ConcordanceWorker(String patternStr, Logic log,
            JSpinner countSpinner, boolean eow, boolean ci, int concStrCount,
            ConcModel model, DefaultTableModel[] concTableModels) {
        this.patternStr = patternStr;
        this.log = log;
        this.countSpinner = countSpinner;
        this.eow = eow;
        this.ci = ci;
        this.concStrCount = concStrCount;
        this.model = model;
        this.concTableModels = concTableModels;
        this.skipMatch = false;
    }

    public ConcordanceWorker(Logic log, int concStrCount,
            ConcModel model, DefaultTableModel[] concTableModels) {

        this.log = log;
        this.concStrCount = concStrCount;
        this.model = model;
        this.concTableModels = concTableModels;
        this.skipMatch = true;
    }

    @Override
    protected Integer doInBackground() throws Exception {

        setProgress(0);
        failIfInterrupted();
        coocs = new ArrayList<>();
        Integer counter = 0;

        if (!skipMatch) {

            ArrayList<Boolean> chosen = log.getChosen();

            for (int i = 0; i < chosen.size(); i++) {

                failIfInterrupted();
                if (chosen.get(i)) {

                    log.getText(i);
                    Matcher match = log.getMatch(patternStr, eow, ci);

                    int index = 0;
                    while (match.find(index)) {

                        failIfInterrupted();
                        Object[] row = log.getConcRow((Integer) countSpinner.getValue(), index);

                        if (row != null) {

                            model.addRow(row);
                            Object[] cooc = log.getCoocStrings(match.start(), match.end(), concStrCount);
                            coocs.add(cooc);

                            counter++;
                        }
                        index = match.end();
                        float percent = (((float) index / (float) log.getTextLength()) * 100) / (log.getFiles().size() - i) / 2;
                        setProgress(Math.round(percent));
                    }
                }
            }
        } else {
            
            ArrayList<Boolean> chosen = log.getChosen();
            
            for (int i = 0; i < chosen.size(); i++) {
                
                if (chosen.get(i)) {
                    
                    log.getText(i);
                    for (int j = 0; j < model.getRowCount(); j++) {
                        
                        String fileName = (String) model.getValueAt(j, 3);
                        if (fileName.equals(log.getFileName())) {
                            
                            int start = (int) model.getValueAt(j, 4);
                            String matchEx = (String) model.getValueAt(j, 1);
                            Object[] cooc = log.getCoocStrings(start, 
                                    start + matchEx.length(), concStrCount);
                            coocs.add(cooc);
                        }
                    }
                }
            }
        }
        setConcTables();
        return counter;
    }

    private void setConcTables() throws InterruptedException {

        for (int i = 0; i < concStrCount * 2; i++) {

            failIfInterrupted();
            ArrayList<ExpressionOccurrence> occurrences = log.getOccurrences(coocs, i);
            for (ExpressionOccurrence occ : occurrences) {
                failIfInterrupted();
                concTableModels[i].addRow(occ.getAsArray());
            }
            float percent = ((float) i / ((float) concStrCount * 2) * 100) / 2;
            setProgress(Math.round(percent) + 50);
        }
    }
}
