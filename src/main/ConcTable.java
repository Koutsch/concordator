/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author koutsch
 */
public class ConcTable extends JTable {

    boolean punctuationConsidered;
    private final LeftDotRenderer beforeRenderer;
    private final DefaultTableCellRenderer concRenderer;
    private final DefaultTableCellRenderer afterRenderer;

    public ConcTable() {

        this.getTableHeader().setReorderingAllowed(false);

        beforeRenderer = new LeftDotRenderer();
        concRenderer = new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                    boolean hasFocus,
                    int row, int column) {
                Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                cell.setFont(new Font(cell.getFont().getFontName(), Font.BOLD, 16));

                if (table.isRowSelected(row)) {

                    setForeground(new Color(0, 0, 0));
                    setBackground(new Color(255, 214, 51));
                } else {
                    setForeground(new Color(0, 0, 0));
                    setBackground(new Color(255, 255, 133));
                }

                return cell;
            }
        };
        afterRenderer = new DefaultTableCellRenderer();

        this.setRowHeight(getFont().getSize() + 6);
        this.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 14));
    }

    /**
     * Set the row sorters for the result table. If the punctuation considered
     * check box is checked non-alphanumeric character are taken into
     * consideration when sorting the table rows.
     *
     * @param punCons
     */
    public void setRowSorters(boolean punCons) {

        punctuationConsidered = punCons;

        TableRowSorter<TableModel> sorter = new TableRowSorter<>();
        this.setRowSorter(sorter);
        sorter.setModel(this.getModel());

        ArrayList<RowSorter.SortKey> list = new ArrayList<>();
        list.add(new RowSorter.SortKey(1, SortOrder.ASCENDING));
        list.add(new RowSorter.SortKey(3, SortOrder.ASCENDING));
        list.add(new RowSorter.SortKey(4, SortOrder.ASCENDING));
        sorter.setSortKeys(list);
        sorter.sort();

        Comparator<String> cmpLeft = new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                String[] strings1 = s1.split("\\s");
                String[] strings2 = s2.split("\\s");

                String string1 = "";
                String string2 = "";

                if (punctuationConsidered == false) {

                    for (int i = strings1.length; i > 0; i--) {

                        Pattern pattern = Pattern.compile("[\\p{InCombiningDiacriticalMarks}|\\p{L}]", Pattern.UNICODE_CHARACTER_CLASS);
                        Matcher matcher = pattern.matcher(strings1[i - 1]);
                        if (matcher.find()) {

                            string1 = strings1[i - 1];
                            break;
                        }
                    }

                    for (int i = strings2.length; i > 0; i--) {

                        Pattern pattern = Pattern.compile("[\\p{InCombiningDiacriticalMarks}|\\p{L}]", Pattern.UNICODE_CHARACTER_CLASS);
                        Matcher matcher = pattern.matcher(strings2[i - 1]);
                        if (matcher.find()) {

                            string2 = strings2[i - 1];
                            break;
                        }
                    }
                } else {

                    string1 = strings1[strings1.length - 1];
                    string2 = strings2[strings2.length - 1];
                }

                Collator col = Collator.getInstance();
                int compare = col.compare(string1, string2);
                return compare;
            }
        };
        sorter.setComparator(0, cmpLeft);

        Comparator<String> cmpRight = new Comparator<String>() {

            @Override
            public int compare(String t, String t1) {
                String[] strings1 = t.split("\\s");
                String[] strings2 = t1.split("\\s");

                String string1 = "";
                String string2 = "";

                if (punctuationConsidered == false) {

                    for (String st : strings1) {
                        Pattern pattern = Pattern.compile("[\\p{InCombiningDiacriticalMarks}|\\p{L}]", Pattern.UNICODE_CHARACTER_CLASS);
                        Matcher matcher = pattern.matcher(st);
                        if (matcher.find()) {
                            string1 = st;
                            break;
                        }
                    }
                    for (String st : strings2) {
                        Pattern pattern = Pattern.compile("[\\p{InCombiningDiacriticalMarks}|\\p{L}]", Pattern.UNICODE_CHARACTER_CLASS);
                        Matcher matcher = pattern.matcher(st);
                        if (matcher.find()) {
                            string2 = st;
                            break;
                        }
                    }
                } else {

                    if (strings1[0].isEmpty()) {

                        string1 = strings1[1];
                    } else {

                        string1 = strings1[0];
                    }

                    if (strings2[0].isEmpty()) {

                        string2 = strings2[1];
                    } else {

                        string2 = strings2[0];
                    }
                }

                Collator col = Collator.getInstance();
                int compare = col.compare(string1, string2);
                return compare;
            }
        };
        sorter.setComparator(2, cmpRight);

//        Comparator<Integer> cmpFile = new Comparator<>() {
//
//            @Override
//            public int compare(Integer t, Integer t1) {
//                return t - t1;
//            }
//        };
//        sorter.setComparator(4, cmpFile);
    }

    public void setRenderers() {

        concRenderer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        beforeRenderer.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        afterRenderer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        TableColumn concColumn = this.getColumnModel().getColumn(1);
        concColumn.setCellRenderer(concRenderer);
        TableColumn beforeColumn = this.getColumnModel().getColumn(0);
        beforeColumn.setCellRenderer(beforeRenderer);
        TableColumn afterColumn = this.getColumnModel().getColumn(2);
        afterColumn.setCellRenderer(afterRenderer);
    }

    public void setColumns() {

        this.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        TableColumnAdjuster tca = new TableColumnAdjuster(this);
        tca.adjustColumns();
    }

    public void changeDirection(boolean rtl) {

        if (rtl) {

            afterRenderer.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
            beforeRenderer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        } else {

            afterRenderer.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
            beforeRenderer.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        }

        TableColumn column[] = new TableColumn[columnModel.getColumnCount()];
        for (int i = column.length - 1; i >= 0; i--) {

            column[i] = columnModel.getColumn(i);
            columnModel.removeColumn(columnModel.getColumn(i));
        }

        TableColumn tmp = column[0];
        column[0] = column[2];
        column[2] = tmp;

        for (TableColumn tc : column) {
            columnModel.addColumn(tc);
        }
    }
}
