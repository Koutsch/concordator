/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author koutsch
 */
public class ConcModel extends DefaultTableModel {

    @Override
    public boolean isCellEditable(int row, int column) {
        return true;   //This causes all cells to be not editable
    }

    public void prepareColumns() {

        this.setRowCount(0);
        this.setColumnCount(0);

        addColumn("Before");
        addColumn("Searched");
        addColumn("After");
        addColumn("File");
        addColumn("Index");
    }

    // makes that words are ordered alphabetically and occurrences acc. to their number
    @Override
    public Class getColumnClass(int column) {

        switch (column) {

            case 0:
            case 1:
            case 2:
            case 3:
            default:
                return String.class;
            case 4:
                return Integer.class;
        }
    }
}
