/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dialogs;

import beans.ExpressionOccurrence;
import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultRowSorter;
import javax.swing.JOptionPane;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import main.ConcModel;
import main.ConcTable;
import main.MainFrame;

/**
 *
 * @author koutsch
 */
public class StatisticsFrame extends javax.swing.JFrame {

    private final MainFrame parent;
    private final DefaultTableModel occModel = new DefaultTableModel() {
        @Override
        public Class getColumnClass(int column) {
            switch (column) {
                case 0:
                default:
                    return String.class;
                case 1:
                    return Integer.class;
            }
        }

        @Override
        public boolean isCellEditable(int row, int column) {
            return false;   //This causes all cells to be not editable
        }
    };
    private final String labelText = "Total count of types: ";
    private final ConcModel concModel = new ConcModel();
    ConcTable concTable;

    /**
     * Creates new form StatisticsFrame
     * @param parent
     * @param statistics
     */
    public StatisticsFrame(MainFrame parent, ArrayList<ExpressionOccurrence> statistics) {

        this.parent = parent;
        initComponents();

        // set concTable
        concTable = new ConcTable();
        concTable.setModel(concModel);
        scrollPaneDown.setViewportView(concTable);

        splitPane.setDividerLocation((splitPane.getHeight() / 4) * 3);

        getStatistics(statistics);
        this.setVisible(true);
    }

    public final void getStatistics(ArrayList<ExpressionOccurrence> statistics) {
        
        occModel.setRowCount(0);
        occModel.setColumnCount(0);
        
        occTable.setAutoCreateRowSorter(true);
        occModel.addColumn("Expression");
        occModel.addColumn("Occurrences");

        DefaultRowSorter<?, ?> sorter = ((DefaultRowSorter) occTable.getRowSorter());
        ArrayList<RowSorter.SortKey> list = new ArrayList<>();
        list.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
        sorter.setSortKeys(list);
        sorter.sort();

        int count = 0;
        for (ExpressionOccurrence occurrence : statistics) {

            occModel.addRow(occurrence.getAsArray());
            count++;
        }

        occTable.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 14));

        countLabel.setText(labelText + count);
    }

    // has to make recalc of the coocs!!!
    private void deleteSelected() throws IOException {

        int[] selectedRows = occTable.getSelectedRows();
        if (selectedRows.length > 0) {

            String[] exclude = new String[selectedRows.length];
            for (int i = selectedRows.length; i > 0; i--) {

                int convertRowIndexToModel = occTable.convertRowIndexToModel(selectedRows[i - 1]);
                exclude[i - 1] = occModel.getValueAt(convertRowIndexToModel, 0).toString();
                occModel.removeRow(convertRowIndexToModel);
            }
            
            parent.deleteSelected(exclude);
        } else {

            JOptionPane.showMessageDialog(this, "Select a type to delete!");
        }
    }

    private void showConcs() {

        int[] selectedRows = occTable.getSelectedRows();
        if (selectedRows.length > 0) {

            concModel.prepareColumns();

            ConcModel parentTableModel = parent.getTableModel();
            int rowCount = parentTableModel.getRowCount();

            for (int i = 0; i < selectedRows.length; i++) {

                Object valueOcc = occModel.getValueAt(occTable.convertRowIndexToModel(selectedRows[i]), 0);

                for (int j = 0; j < rowCount; j++) {

                    Object valueAt = parentTableModel.getValueAt(j, 1);

                    if (valueOcc.equals(valueAt)) {

                        int columnCount = parentTableModel.getColumnCount();
                        Object[] col = new Object[columnCount];
                        for (int k = 0; k < columnCount; k++) {

                            col[k] = parentTableModel.getValueAt(j, k);
                        }

                        concModel.addRow(col);
                    }
                }
            }
            concTable.setColumns();
            concTable.setRowSorters(false);
            concTable.setRenderers();

            rToLToggleButton.setEnabled(true);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        countLabel = new javax.swing.JLabel();
        rToLToggleButton = new javax.swing.JToggleButton();
        okButton = new javax.swing.JButton();
        delSelButton = new javax.swing.JButton();
        showButton = new javax.swing.JButton();
        splitPane = new javax.swing.JSplitPane();
        scrollPaneUp = new javax.swing.JScrollPane();
        occTable = new javax.swing.JTable();
        scrollPaneDown = new javax.swing.JScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Statistics");

        countLabel.setText("STAT");

        rToLToggleButton.setText("العربية עברית ‎");
        rToLToggleButton.setEnabled(false);
        rToLToggleButton.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rToLToggleButtonItemStateChanged(evt);
            }
        });

        okButton.setText("OK");
        okButton.setToolTipText("Close");

        delSelButton.setText("Del. Sel.");
        delSelButton.setToolTipText("Delete Selection");
        delSelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delSelButtonActionPerformed(evt);
            }
        });

        showButton.setText("Show Sel.");
        showButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showButtonActionPerformed(evt);
            }
        });

        splitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        occTable.setModel(occModel);
        scrollPaneUp.setViewportView(occTable);

        splitPane.setLeftComponent(scrollPaneUp);
        splitPane.setBottomComponent(scrollPaneDown);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(splitPane, javax.swing.GroupLayout.DEFAULT_SIZE, 535, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(countLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rToLToggleButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(showButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(delSelButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(splitPane, javax.swing.GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(countLabel)
                    .addComponent(okButton)
                    .addComponent(delSelButton)
                    .addComponent(showButton)
                    .addComponent(rToLToggleButton))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rToLToggleButtonItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rToLToggleButtonItemStateChanged
        // TODO add your handling code here:
        concTable.changeDirection(rToLToggleButton.isSelected());
    }//GEN-LAST:event_rToLToggleButtonItemStateChanged

    private void delSelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delSelButtonActionPerformed
        try {
            // TODO add your handling code here:
            deleteSelected();
        } catch (IOException ex) {
            Logger.getLogger(StatisticsFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_delSelButtonActionPerformed

    private void showButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showButtonActionPerformed
        // TODO add your handling code here:
        showConcs();
    }//GEN-LAST:event_showButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel countLabel;
    private javax.swing.JButton delSelButton;
    private javax.swing.JTable occTable;
    public javax.swing.JButton okButton;
    private javax.swing.JToggleButton rToLToggleButton;
    private javax.swing.JScrollPane scrollPaneDown;
    private javax.swing.JScrollPane scrollPaneUp;
    private javax.swing.JButton showButton;
    private javax.swing.JSplitPane splitPane;
    // End of variables declaration//GEN-END:variables
}
