/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

/**
 *
 * @author jawiedne
 */
    public class ExpressionOccurrence {

        private String expression;
        private int count;

        public ExpressionOccurrence() {
        }

        public ExpressionOccurrence(String expression, int count) {
            this.expression = expression;
            this.count = count;
        }

        public String getExpression() {
            return expression;
        }

        public int getCount() {
            return count;
        }

        public void setExpression(String expression) {
            this.expression = expression;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public void increaseCount(int toAdd) {

            count += toAdd;
        }

        public Object[] getAsArray() {

            Object[] arr = new Object[2];
            arr[0] = expression;
            arr[1] = count;
            return arr;
        }
    }
